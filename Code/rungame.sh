#! /bin/bash

cd `dirname $0`

TEAM1=`basename $1`
TEAM2=`basename $2`
TEAM1DIR=$1
TEAM2DIR=$2
ROUND="${3:-x}"

source defaults.conf

basedir=`dirname $0`/../
basedir=`realpath ${basedir}`
leaguedir="${leaguedir:-$basedir}"
logdir="${logdir:-$leaguedir/logs${ROUND}}"
archivedir="${archivedir:-$leaguedir/archive}"

SERVER=localhost

mkdir -p ${logdir}
mkdir -p ${archivedir}

if test "$#" -ne "3"; then
    echo "Error: Wrong number of arguments"
    echo ""
    echo "Usage: $0 TEAM1DIR TEAM2DIR ROUND"
    echo ""
    exit 2
fi

if test -f "${leaguedir}/stop"; then
    echo "Stop file is present."
    echo "To restart league, remove the stop file and run loopleague"
    exit 3
fi

GAME=`echo "${TEAM1} vs ${TEAM2}"`

echo "Running ${GAME}..."

team1start="server::team_l_start = \"${TEAM1DIR}/start.sh ${SERVER} ${TEAM1DIR}\""
team2start="server::team_r_start = \"${TEAM2DIR}/start.sh ${SERVER} ${TEAM2DIR}\""
automode="server::auto_mode = on"

if ! test -x "${TEAM1DIR}/start.sh"; then
    echo "File \"${TEAM1DIR}/start.sh\" not found"
    echo ""
    exit 0;
fi

if ! test -x "${TEAM2DIR}/start.sh"; then
    echo "File \"${TEAM2DIR}/start.sh\" not found"
    echo ""
    exit 0;
fi

    
serverlogdirs="server::game_log_dir = ${logdir} server::text_log_dir = ${logdir} server::body_log_dir = ${logdir} server::visuals_log_dir = ${logdir}"

LOGTIME=`date "+%Y%m%d%H%M"`
LOGNAME=`echo "$LOGTIME-$TEAM1-vs-$TEAM2.log"`

ls ${logdir}/*-${TEAM1}-vs-${TEAM2}.log >& /dev/null
EXIT=$?
if test $EXIT -eq 0; then
    echo "Found existing log."
    echo "Assuming game has already been played."
    echo ""
else
    # echo rcssserver ${team1start} ${team2start} ${automode} ${serverxtrapar} ${serverlogdirs} \
    # server::nr_extra_halfs = 0 server::penalty_shoot_outs = 0 \
    # server::half_time = 300 

    rcssserver ${team1start} ${team2start} ${automode} ${serverxtrapar} ${serverlogdirs} \
    server::nr_extra_halfs = 0 server::penalty_shoot_outs = 0 \
    server::half_time = 300 >& "${logdir}/${LOGNAME}"
    if test $? -ne 0; then
        echo "...${GAME} killed"
        exit 1
    fi
    echo "...${GAME} finished"
    echo ""
fi

LOGS=`ls ${logdir}/*.rcg*`
EXIT=$?
if test $EXIT -ne 0; then
    echo "Error: Could not find rcg log"
    exit 1
fi

rcgfile=`ls -1t ${logdir}/*.rcg | head -1`
RESULT1=`echo $rcgfile | sed  's/.*_\(.*\)\-vs\-.*/\1/'`
RESULT2=`echo $rcgfile | sed  's/.*\-vs\-\(.*\)_\(.*\)\.rcg/\2/'`

./archive.sh ${LOGTIME} ${TEAM1} ${TEAM2} ${RESULT1} ${RESULT2} ${ROUND}

exit 0
