#! /bin/bash

cd `dirname $0`

for round in {1..12}
do
    echo "Starting new league, round ${round}"
    ./runleague.sh ${round}
done

echo "all rounds done"
