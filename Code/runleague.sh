#! /bin/bash

cd `dirname $0`

ROUND="${1:-xx}"

source defaults.conf

basedir=`dirname $0`/../
basedir=`realpath ${basedir}`
leaguedir="${leaguedir:-$basedir}"

if ! test -e ${leaguedir}/teams; then
    echo "Error: Could not find teams file"
    echo ""
    exit 1
fi

DIRLIST=`< $leaguedir/teams`

echo "Starting League..."
echo ""
echo ${DIRLIST}

touch ${leaguedir}/completed.${ROUND}

FIRST=true;

for team1 in ${DIRLIST}; do
    for team2 in ${DIRLIST}; do
        if test "${team1}" != "${team2}"; then
            COMPLETED=`cat ${leaguedir}/completed.${ROUND}`
            COMP_NAME=`echo "${team1}-vs-${team2}"`
            TEAM1=`basename ${team1}`
            TEAM2=`basename ${team2}`
            GAME=`echo "${TEAM1}-vs-${TEAM2}"`
            GAMERUN=false

            for g in ${COMPLETED}; do

                if test "${COMP_NAME}" = "${g}"; then
                    echo "Running ${g}..."
                    echo "...${g} has already been run";
                    echo ""
                    GAMERUN=true
                fi
            done
            if ! ${GAMERUN}; then 
                FIRST=false;
                ./rungame.sh ${team1} ${team2} ${ROUND}
                EXIT=$?
                if test ${EXIT} -ne 0; then
                    echo "...Stopping League"
                    exit 1
                else
                    if test "${leaguedir}/teams" -nt "${leaguedir}/completed.${ROUND}"; then
                        echo "${COMP_NAME}" >> $leaguedir/completed.${ROUND}
                        echo "Teams has been edited.  Restarting league"
                        exec ./runleague.sh
                    fi
                    echo "${COMP_NAME}" >> $leaguedir/completed.${ROUND}
                fi
            fi
        fi
    done
done


EXIT=$?
if test ${EXIT} -ne 0; then
    echo "Error adding round"
    exit 1
fi

rm -f ${leaguedir}/completed.${ROUND}

echo "...League (round ${ROUND}) finished"
echo ""
