#! /bin/bash

LOGTIME=$1
TEAM1=$2
TEAM2=$3
RESULT1=$4
RESULT2=$5
ROUND="${6:-0}"

source defaults.conf

basedir=`dirname $0`/../
basedir=`realpath ${basedir}`
leaguedir="${leaguedir:-$basedir}"
logdir="${logdir:-$leaguedir/logs$ROUND}"
archivedir="${archivedir:-$leaguedir/archive}"

dest="${archivedir}/${TEAM1}/${TEAM2}/${ROUND}"
mkdir -p ${dest}

sources=`ls -1 "${logdir}/${LOGTIME}"*.*`

mv ${sources} ${dest}/

echo "${ROUND}, ${LOGTIME}, ${TEAM1}, ${TEAM2}, ${RESULT1}, ${RESULT2}" >> ${archivedir}/${TEAM1}/results${ROUND}-${TEAM1}.csv
